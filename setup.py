# See setup.cfg for the package metadata.
import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(long_description=long_description)
